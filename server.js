var express = require('express');  // require express
var app = express();  // create a request handler function
var server = require('http').createServer(app);  // use our app to create a server
//var io = require('socket.io')(server); // pass in our http app server to get a Socket.io server
var path = require('path');
var logger = require("morgan");
var bodyParser = require("body-parser");
 
// on a GET request to default page, do this.... 
app.get('/', function(req, res){
  app.use(express.static(path.join(__dirname)));
  res.sendFile(path.join(__dirname, '../A03Kankara/assets', 'About.html'));
});

app.get('/About', function(req, res){
  app.use(express.static(path.join(__dirname)));
  res.sendFile(path.join(__dirname, '../A03Kankara/assets', 'About.html'));
});

app.get('/Contact', function(req, res){
  app.use(express.static(path.join(__dirname)));
  res.sendFile(path.join(__dirname, '../A03Kankara/assets', 'Contact.html'));
});
 
 app.get('/Grocery_Bill_Estimator', function(req, res){
  app.use(express.static(path.join(__dirname)));
  res.sendFile(path.join(__dirname, '../A03Kankara/assets', 'Grocery_Bill_Estimator.html'));
});
app.use(express.static(__dirname + '/assets'));

app.set("views", path.resolve(__dirname, "views")); // path to views
app.set("view engine", "ejs"); // specify our view engine

// create an array to manage our entries
var entries = [];
app.locals.entries = entries;

// set up the http request logger
app.use(logger("dev"));
app.use(bodyParser.urlencoded({ extended: false }));

// http GET (default and /new-entry)
app.get("/index", function (request, response) {
  response.render("index");
});
app.get("/new-entry", function (request, response) {
  response.render("new-entry");
});

// http POST (INSERT)
app.post("/new-entry", function (request, response) {
  if (!request.body.title || !request.body.body) {
    response.status(400).send("Entries must have a title and a body.");
    return;
  }
  entries.push({
    title: request.body.title,
    content: request.body.body,
    published: new Date()
  });
  response.redirect("/index");
});

// 404
//app.use(function (request, response) {
 // response.status(404).render("404");
//});

// on a connection event, act as follows (socket interacts with client)
/*io.on('connection', function(socket){ 
  socket.on('chatMessage', function(from, msg){  // on getting a chatMessage event
    io.emit('chatMessage', from, msg);  // emit it to all connected clients
  });
  socket.on('notifyUser', function(user){  // on getting a notifyUser event
    io.emit('notifyUser', user);  // emit to all 
  });
}); */
 
// Listen for an app request on port 8081
server.listen(8081, function(){
  console.log('listening on http://127.0.0.1:8081/');
});